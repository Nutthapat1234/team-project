from enum import Enum
import random as rm


class WaveB(Enum):
    B50 = 0.500
    B61 = 0.618
    B76 = 0.764
    B85 = 0.854


class WaveC(Enum):
    C61 = 0.618
    C100 = 1.00
    C161 = 1.618


# for Pattern5
class WaveA(Enum):
    A38 = 0.382


def random(waveClass):
    return rm.choice([c.value for c in waveClass])
