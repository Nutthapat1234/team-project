from abc import ABC, abstractmethod
from ElliotWavePattern.ratio import WaveB, WaveC


class Pattern(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def generate(self):
        pass

    @abstractmethod
    def generateFromUp(self, start_point, length):
        pass

    @abstractmethod
    def generateFromDown(self, start_point, length):
        pass
