from ElliotWavePattern.pattern2 import Pattern2
from ElliotWavePattern.downtrade import DownTrade
from ElliotWavePattern.pattern1 import Pattern1
import matplotlib.pyplot as plt
import pandas as pd

from ElliotWavePattern.pattern5 import Pattern5
from ElliotWavePattern.uptrade import UpTrade


def plot(df):
    ax = plt.gca()
    df.index += 1
    df.reset_index().plot(kind='line', x='index', y="value", ax=ax)
    plt.show()


def normalize(max_value, min_value, data):
    max_scale = 1
    min_scale = 0

    normalize_data = []
    for item in data:
        value = min_scale + ((item - min_value) * (max_scale - min_scale)) / (max_value - min_value)
        normalize_data.append(value)

    return normalize_data


def export(dataframe, filename):
    dataframe.to_csv(filename, index=False)


def make_normalized(p, code):
    value = p["value"]
    max_point = max(value)
    min_point = min(value)
    if max_point == min_point:
        plot(pd.DataFrame(p))
    normalized = normalize(max_point, min_point, value)
    for i in range(len(normalized)):
        normalized[i] = round(normalized[i], 2)

    p["value"] = normalized + [str(code)]


def pattern_generate(num, code):
    pattern = None
    if code == 1:
        pattern = Pattern1()
    elif code == 2:
        pattern = Pattern2()
    else:
        pattern = Pattern5()

    data = None
    for i in range(num):
        temp_pattern = pattern.generate()
        make_normalized(temp_pattern, code)

        if data is None:
            data = pd.DataFrame(temp_pattern)
            data.index += 1
            data = data.transpose()
            continue

        temp_pd = pd.DataFrame(temp_pattern)
        temp_pd.index += 1
        temp_pd = temp_pd.transpose()
        data = pd.concat([temp_pd, data])

    data.rename(columns={11: "Pattern"}, inplace=True)
    return data


if __name__ == '__main__':
    pattern1 = pattern_generate(1000, 1)
    export(pattern1, "Pattern1.csv")

    pattern2 = pattern_generate(1000, 2)
    export(pattern2, "Pattern2.csv")

    pattern5 = pattern_generate(1000, 5)
    export(pattern5, "Pattern5.csv")
