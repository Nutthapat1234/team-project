from ElliotWavePattern.pattern import Pattern
import random as rm

from ElliotWavePattern.ratio import WaveB, random, WaveC


class DownTrade(Pattern):
    def __init__(self):
        super().__init__()

    def generate(self):
        start_point = rm.randrange(1, 1000)
        point_a = rm.randrange(0, start_point)
        while point_a == start_point:
            point_a = rm.randrange(0, start_point)

        length = abs(start_point - point_a)

        point_b = point_a + (random(WaveB) * length)
        point_c = point_b - (random(WaveC) * length)

        value = [start_point, point_a, point_b, point_c]

        data = {
            "value": value,
            "end_point": point_c,
            "length": abs(start_point - point_c)}

        return data

    # last wave for Wave C
    def generateFromUp(self, start_point, length):
        ratio_b = random(WaveB)
        ratio_c = random(WaveC)
        point_c = start_point - (random(WaveC) * length)

        length_a = (point_c - start_point) / (ratio_b - ratio_c - 1)
        point_a = start_point - length_a
        point_b = point_a + (ratio_b * length_a)

        value = [point_a, point_b, point_c]

        data = {
            "value": value,
            "end_point": point_c,
            "length": abs(start_point - point_c)
        }

        return data

    def generateFromDown(self, start_point, length):
        pass
