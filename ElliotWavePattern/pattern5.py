from ElliotWavePattern.downtrade import DownTrade
from ElliotWavePattern.pattern import Pattern
from ElliotWavePattern.ratio import WaveA, random, WaveB
from ElliotWavePattern.uptrade import UpTrade


class Pattern5(Pattern):
    def __init__(self):
        super().__init__()
        self.up = UpTrade()
        self.down = DownTrade()

    def generate(self):
        wave_a = self.up.generate()

        start_point = wave_a["end_point"]
        length_a = WaveA.A38.value * wave_a["length"]
        point_a = start_point - length_a
        point_b = point_a + (random(WaveB) * length_a)
        point_c = point_b - length_a
        wave_b = {
            'value': [point_a, point_b, point_c]
        }

        wave_c = self.up.generateFromDown(point_c, wave_a["length"])

        value = wave_a["value"] + wave_b["value"] + wave_c["value"]

        return {
            "value": value
        }

    def generateFromUp(self, start_point, length):
        pass

    def generateFromDown(self, start_point, length):
        pass
