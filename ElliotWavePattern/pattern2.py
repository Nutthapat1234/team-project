from ElliotWavePattern.downtrade import DownTrade
from ElliotWavePattern.pattern import Pattern
from ElliotWavePattern.uptrade import UpTrade


class Pattern2(Pattern):
    def __init__(self):
        super().__init__()
        self.up = UpTrade()
        self.down = DownTrade()

    def generate(self):
        wave_a = self.up.generate()
        wave_b = self.down.generateFromUp(wave_a["end_point"], wave_a["length"])
        wave_c = self.up.generateFromDown(wave_b["end_point"], wave_a["length"])

        value = wave_a["value"] + wave_b["value"] + wave_c["value"]

        return {
            "value": value
        }

    def generateFromUp(self, start_point, length):
        pass

    def generateFromDown(self, start_point, length):
        pass
