from ElliotWavePattern.downtrade import DownTrade
from ElliotWavePattern.pattern import Pattern
import random as rm
import pandas as pd
import numpy as np

from ElliotWavePattern.uptrade import UpTrade


class Pattern1(Pattern):
    def __init__(self):
        super().__init__()
        self.up = UpTrade()
        self.down = DownTrade()

    def generate(self):
        wave_a = self.down.generate()
        wave_b = self.up.generateFromDown(wave_a["end_point"], wave_a["length"])
        wave_c = self.down.generateFromUp(wave_b["end_point"], wave_a["length"])

        value = wave_a["value"] + wave_b["value"] + wave_c["value"]

        return {
            "value": value
        }

    def generateFromUp(self, start_point, length):
        pass

    def generateFromDown(self, start_point, length):
        pass
